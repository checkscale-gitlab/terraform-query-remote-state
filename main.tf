# Create a random pet name for suffix
resource "random_pet" "petname" {
}

locals {
  petname = random_pet.petname.id
}

# Create a bucket
module "bucket" {
  source  = "terraform-google-modules/cloud-storage/google//modules/simple_bucket"
  version = "~> 1.6"

  name       = "remote-state-${local.petname}"
  project_id = var.project-id
  location   = var.region
}
